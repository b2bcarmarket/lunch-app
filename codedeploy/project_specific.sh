#!/bin/bash -e

DEPLOY_FOLDER=/var/www/${DEPLOYMENT_GROUP_NAME}/${APPLICATION_NAME}



CONFIG_FILE=$(aws ssm get-parameter --name /${APPLICATION_NAME}/${DEPLOYMENT_GROUP_NAME}  | jq -r .Parameter | jq -r .Value)
echo "$CONFIG_FILE" > ${DEPLOY_FOLDER}/.env
cd ${DEPLOY_FOLDER}
php7.4 artisan storage:link
php7.4 artisan migrate
#php7.4 artisan db:seed
