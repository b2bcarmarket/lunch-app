<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Restaurant;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/restaurants', 'RestaurantController@getRestaurants');
Route::get('/restaurants/{id}/menu-items', 'RestaurantController@getMenuItems');

Route::post('/users/login', 'LoginController@login');
Route::post('/users/check', 'UserController@checkUser');

Route::group(['middleware' => ['auth.jwt']], function(){
    Route::get('/users/orders', 'OrderController@allTodaysOrders');
    Route::put('/users/{id}/orders', 'OrderController@addOrder');
    Route::delete('/users/orders/{id}', 'OrderController@deleteOrder');
    Route::patch('/users/{id}/orders', 'OrderController@completeOrder');

    // Change users password
    Route::patch('/users/password', 'UserController@updatePassword');

    Route::group(['middleware' => ['checkAdmin']], function(){
        Route::delete('/users/{id}', 'UserController@deleteUser');
        Route::patch('/users', 'UserController@updateUsers');
        Route::post('/users', 'UserController@addUser');
        Route::get('/users', 'UserController@getUsers');
    });
});
