<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App
 *
 * @property int id
 * @property int is_finished
 * @property int restaurant_id
 * @property int finished_user_id
 *
 */
class Order extends Model
{
    protected $dateFormat = "Y-m-d";

    protected $fillable = [
        'finished_user_id', 'is_finished'
    ];

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function create(int $restaurantId)
    {
        $this->restaurant_id = $restaurantId;
        $this->save();

        return $this->first();
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class)->first();
    }
}
