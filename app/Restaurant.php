<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Restaurant
 * @package App
 *
 * @property int id
 * @property string name
 * @property string phone_number
 * @property string url
 * @property bool was_updated
 * @property MenuItem menuItem
 */
class Restaurant extends Model
{
    protected $fillable = [
        'name'
    ];

    // Used to display menu items from selected restaurant
    public function menuItems()
    {
        return $this->hasMany(MenuItem::class)->latest();
    }
}
