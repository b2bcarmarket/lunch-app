<?php

namespace App\Http\Controllers\restaurantsScraper;

use Carbon\Carbon;
use Goutte\Client;
use App\MenuItem;
use App\Http\Controllers\ScraperController;

class FemecscraperController extends ScraperController
{
    /**
     * Gets today's day in slovene because of scrapper
     */
    public function todayDay(): string
    {
        $day = Carbon::now()->locale('sl')->dayName;
        return str_replace(array('č'), array('c'), $day);
    }

    public function checkIfAlreadyInserted($id): bool
    {
        return MenuItem::where("restaurant_id", $id)
            ->whereDate('created_at', Carbon::now())->exists();
    }

    public function addDataToDatabase(array $items, $id)
    {
        foreach ($items as $item) {
            $menuItem = new MenuItem();
            $menuItem->item = $item;
            $menuItem->restaurant_id = $id;

            $menuItem->save();
        }
    }

    public function scrapTodaysItems()
    {
        if ($this->passed && !$this->checkIfAlreadyInserted($this->id)) {

            $client = new Client();
            $todaysDateInSlovene = $this->todayDay();
            $dailyItems = [];
            $checkString = "";

            $scrapper = $client->request('GET', $this->url);

            // Checks if data that was return contains anything
            // If not then it returns false and stops function
            if ($scrapper == null) {
                return false;
            }

            // Gets websites data and explodes string that was returned,
            // and stores exploded data to array
            $scrapper->filter('#wholemenu > #dayscontent > #' . $todaysDateInSlovene . ' > .menuline > .menucontent')->each(function ($node) use (&$dailyItems, &$checkString) {
                array_push($dailyItems, $node->text());
                $checkString .= $node->text();
            });

            if ($this->appendDataToJSON($checkString)) {
                return false;
            }

            // Removes the last item from an array, since the last item contains
            // multiple items that will be separately scraped and stored to the
            array_pop($dailyItems);
            $lastIndex = count($dailyItems) + 1;

            $scrapper->filter('#wholemenu > #dayscontent > #' . $todaysDateInSlovene . ' > .menuline > .menu' . $lastIndex . ' > p')->each(function ($node, $i) use (&$dailyItems) {
                if ($i != 0) {
                    array_push($dailyItems, $node->text());
                }
            });

            $this->dailyItems = $dailyItems;

            $this->addDataToDatabase($this->dailyItems, $this->id);
        }

    }

    public function appendDataToJSON(string $newdata): bool
    {
        $data = file_get_contents($this->JsonFile);
        $decodedData = json_decode($data, true);

        if ($newdata === $decodedData[$this->restaurantName][0]) {
            return true;
        }

        $decodedData[$this->restaurantName][0] = $newdata;
        $jsonData = json_encode($decodedData);
        file_put_contents($this->JsonFile, $jsonData);
        return false;
    }
}
