<?php

namespace App\Http\Controllers\restaurantsScraper;

use Carbon\Carbon;
use Goutte\Client;
use App\Http\Controllers\ScraperController;
use App\MenuItem;

class VinkascraperController extends ScraperController
{
    public function checkIfAlreadyInserted($id): bool
    {
        return MenuItem::where("restaurant_id", $id)
            ->whereDate('created_at', Carbon::now())->exists();
    }

    public function addDataToDatabase(array $items, $id): bool
    {

        for ($i = 0; $i < count($items); $i++) {
            $menuItem = new MenuItem();

            $menuItem->item = $items[$i]["data"];
            $menuItem->price = floatVal($items[$i]["price"]);
            $menuItem->restaurant_id = $id;

            $menuItem->save();
        }
        return 1;
    }

    public function scrapTodaysItems()
    {

        if ($this->passed && !$this->checkIfAlreadyInserted($this->id)) {

            $client = new Client();
            $dailyItems = [];
            $content = 'div[role=content] > p';

            $scrapper = $client->request('GET', $this->url);

            // Checks if data that was return contains anything
            // If not then it returns false and stops function
            if ($scrapper == null) {
                return false;
            }

            // Checks if newly scraped data is the same as yesterday's data
            if ($this->appendDataToJSON($scrapper->filter($content)->text())) {
                return false;
            }

            $scrapper->filter($content)->each(function ($node) use (&$dailyItems) {
                array_push($dailyItems, explode("eur", $node->text()));
            });

            // Removes last item
            array_pop($dailyItems[0]);

            // Loops through array and splits items that include ':' (example, Solatni krozniki:, Dobre stare stalnice:, Fit ponudba:,..)
            // Stores 2nd item to current Restaurants
            for ($i = 0; $i < count($dailyItems[0]); $i++) {
                if (strpos($dailyItems[0][$i], ':') != false) {
                    $newItem = explode(":", $dailyItems[0][$i]);
                    $dailyItems[0][$i] = $newItem[1];
                }
                $data = $dailyItems[0][$i];

                if ($data[strlen($data) - 1] == " ") {
                    $data = substr($data, 0, strlen($data) - 1);
                }

                $price = explode(" ", $data);
                $price = $price[sizeof($price) - 1];
                $price = str_replace(",", ".", $price);

                $dailyItems[0][$i] = array(
                    "price" => $price,
                    "data" => $data,
                    "Restaurants" => $i
                );
            }

            $this->dailyItems = $dailyItems[0];

            $this->addDataToDatabase($this->dailyItems, $this->id);
        }
    }

    public function appendDataToJSON(string $newdata): bool
    {
        $data = file_get_contents($this->JsonFile);

        $decodedData = json_decode($data, true);

        if ($newdata === $decodedData[$this->restaurantName][0]) {
            return true;
        }

        $decodedData[$this->restaurantName][0] = $newdata;
        $jsonData = json_encode($decodedData);
        file_put_contents($this->JsonFile, $jsonData);
        return false;
    }
}
