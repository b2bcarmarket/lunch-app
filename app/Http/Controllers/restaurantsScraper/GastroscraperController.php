<?php

namespace App\Http\Controllers\restaurantsScraper;

use App\MenuItem;
use Goutte\Client;
use App\Http\Controllers\ScraperController;
use Carbon\Carbon;

class GastroscraperController extends ScraperController
{
    public function filterIsValid($filter): bool
    {
        try {
            $filter->text();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function checkIfAlreadyInserted($id): bool
    {
        return MenuItem::where("restaurant_id", $id)
            ->whereDate('created_at', Carbon::now())->exists();
    }

    public function addDataToDatabase(array $items, $id)
    {
        foreach ($items as $item) {
            $menuItem = new MenuItem();
            $menuItem->item = $item["data"];
            $menuItem->price = floatVal($item["price"]);
            $menuItem->restaurant_id = $id;

            $menuItem->save();
        }
    }

    public function scrapTodaysItems()
    {
        if ($this->passed && !$this->checkIfAlreadyInserted($this->id)) {

            $client = new Client();
            $dailyItems = [];
            $scrapper = $client->request('GET', $this->url);

            $filter = $scrapper->filter('.futr > ul > li');

            // Gastro house changes HTML structure every 2 to 3 days. So this condition handles the changes
            if (!$this->filterIsValid($filter)) {
                $filter = $scrapper->filter('.futr > .naslov > ul > li');
                if (!$this->filterIsValid($filter)) {
                    return \response('There was an error trying to obtain data from Gastro');
                }
            }

            // Gets websites data and explodes string that was returned, and stores exploded data to array
            $filter->each(function ($node) use (&$dailyItems) {
                array_push($dailyItems, explode("€", $node->text(), 1));
            });

            // Checks if data that was return contains anything, if not then it returns false and stops function
            if ($scrapper == null) {
                return false;
            }

            // Checks if newly scraped data is the same as yesterday's data
            if ($this->appendDataToJSON($filter->text())) {
                return false;
            }

            // Removes items from an array whose content is all in uppercase.
            foreach ($dailyItems as $key => $value) {
                if (strtoupper($value[0]) == $value[0]) {
                    unset($dailyItems[$key]);
                }
            }

            // Re-increments array Restaurants and then deletes the last item from array
            $dailyItems = array_values($dailyItems);
            array_pop($dailyItems);

            $counter = 0;
            // Formats array, so returned data is displayed and stored correctly
            foreach ($dailyItems as $dailyItem) {
                $data = reset($dailyItem);

                if ($data[strlen($data) - 1] == " ") {
                    $data = substr($data, 0, strlen($data) - 1);
                }

                $price = explode(" ", $data);
                $price = $price[sizeof($price) - 1];
                $price = str_replace(",", ".", $price);


                $dailyItems[$counter] = array(
                    "price" => $price,
                    "data" => rtrim($data, "€"),
                    "Restaurants" => $counter
                );
                $counter++;
            }

            $this->dailyItems = $dailyItems;

            $this->addDataToDatabase($this->dailyItems, $this->id);
        }
    }

    public function appendDataToJSON(string $newdata): bool
    {
        $data = file_get_contents($this->JsonFile);
        $decodedData = json_decode($data, true);

        if ($newdata === $decodedData[$this->restaurantName][0]) {
            return true;
        }

        $decodedData[$this->restaurantName][0] = $newdata;
        $jsonData = json_encode($decodedData);
        file_put_contents($this->JsonFile, $jsonData);
        return false;
    }
}
