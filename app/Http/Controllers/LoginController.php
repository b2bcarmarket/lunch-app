<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    use ThrottlesLogins;

    /**
     * Instead of username, email is being provided
     * @return string
     */
    public function username(): string
    {
        return 'email';
    }

    /**
     * Logs in user if provided credentials are correct
     * @param Request $request credentials that user provides (email, password)
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        // Login throttling, if user provides non-valid credentials he is blocked
        // from making attempts to login for 1 min
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // JWT token is send if credentials are correct
        $credentials = $request->only(['email', 'password']);
        if (!$token = JWTAuth::attempt($credentials)) {
            $this->incrementLoginAttempts($request);
            return response('Provided data was incorrect!', 400);
        } else {

            $token = JWTAuth::attempt($credentials);
            return response()->json([
                "token" => $token,
                "name" => JWTAuth::user()->name,
                "email" => JWTAuth::user()->email,
                "id" => JWTAuth::user()->id
            ]);
        }
    }
}
