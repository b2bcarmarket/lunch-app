<?php

namespace App\Http\Controllers;

use App\MenuItem;
use App\Restaurant;
use Illuminate\Http\JsonResponse;
use phpDocumentor\Reflection\Types\Collection;

class RestaurantController extends Controller
{
    public function getRestaurants()
    {
        return Restaurant::all();
    }

    public function getMenuItems(int $id): JsonResponse
    {
        /** @var Restaurant $restaurant */
        $restaurant = Restaurant::findOrFail($id);
        $items = $restaurant->menuItems;

        if (count($items) > 0) {
            return new JsonResponse([
                "restaurantName" => $restaurant->name,
                "menuItems" => $items
            ]);
        } else if (count($items) == 0) {
            return new JsonResponse([
                "response_text" => "Data was not updated today yet!",
                "restaurantName" => $restaurant->name,
            ]);
        }
        abort(404, "Restaurant does't exist!");
    }
}
