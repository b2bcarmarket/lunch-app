<?php

namespace App\Http\Controllers;

use App\Events\OrderCompleteEvent;
use App\Events\OrderDeleteEvent;
use App\Events\OrdersEvent;
use App\Order;
use App\OrderItem;
use App\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SlackLunch;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\OrderCollection;

class OrderController extends Controller
{

    /**
     * @param null $type
     * @param null $value
     * @return mixed
     */
    protected function returnData($type = null, $value = null)
    {
        switch ($type) {
            case "id":
                return Order::where('order_items.id', $value)->items()
                    ->with('menu_item.restaurant')
                    ->get();
            case "name":
                return OrderItem::select([
                    Db::raw('count(menu_items.item) orders'),
                    'order_items.comment',
                    'menu_items.item',
                    DB::raw('sum(menu_items.price) price')
                ])
                    ->join('menu_items', 'order_items.menu_item_id', 'menu_items.id')
                    ->join('restaurants', 'menu_items.restaurant_id', 'restaurants.id')
                    ->where('restaurants.id', $value)
                    ->where('menu_items.created_at', date('Y-m-d'))
                    ->groupBy('order_items.comment', 'menu_items.item')
                    ->get();
            default:
                return Order::whereDate('created_at', date('Y-m-d'))
                    ->first()
                    ->items()
                    ->with(['user', 'menu_item.restaurant'])
                    ->get();
        }
    }

    /**
     * Returns all current orders
     * @return JsonResponse
     */
    public function allTodaysOrders(): JsonResponse
    {
        $checkIfOrderExists = Order::where('created_at', date('Y-m-d'))->first();

        if ($checkIfOrderExists != null && $checkIfOrderExists->is_finished == 0) {
            return new JsonResponse($this->returnData(), 200);
        }
        return new JsonResponse("No order was made for today", 204);
    }

    /**
     * Adds order
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function addOrder(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $item = (array)$request->input('item', []);
        $comment = (string)$request->input('comment');

        // validate item$restaurantName
        if (empty($item)) {
            throw new \Exception('Item not provided!');
        }
        // get or create order
        /** @var Order $order */
        $order = Order::where('created_at', date("Y-m-d"))->first();

        if (!$order) {
            $order = (new Order())->create((int)$item['restaurant_id']);
        }
        //check if order is still active
        if ($order->is_finished) {
            abort(403, 'Order already finished');
        }

        $orderItem = $order->items()
            ->where('user_id', $userId)
            ->exists();
        if ($orderItem) {
            abort(403, 'You already ordered.');
        }

        $orderItem = (new OrderItem())->fill([
            'comment' => $comment,
            'user_id' => $userId,
            'menu_item_id' => $item['id']
        ]);

        $order->items()->save($orderItem);

        // Broadcasts new data
        broadcast(new OrdersEvent($this->returnData(), "orders"));

        return new JsonResponse([
            'message' => 'You placed an order'
        ], 201);
    }


    /**
     * Submit today's order
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function completeOrder(Request $request): JsonResponse
    {
        $userId = (int)Auth::id();
        $restaurantId = (int)$request->input('restaurantName');

        $restaurantName = Restaurant::select('name')->where('id', $restaurantId)->first();


        if (empty($restaurantId)) {
            throw new \Exception('Restaurant was not provided!');
        }

        // Updates order
        $order = Order::where('created_at', date('Y-m-d'))->first();

        $submitOrder = $order->update(
            ['finished_user_id' => $userId,
                'is_finished' => 1]
        );

        // if submission was successful
        if ($submitOrder) {
            $data = $this->returnData("name", $restaurantId);

            $items = [];

            // Formats data to be stored and then send to Slack
            foreach ($data as $line) {
                array_push($items, sprintf("_%s - %s€ %s_", $line->orders, $line->item, $line->comment ? sprintf("comment: %s", $line->comment) : ""));
            }

            $items = implode("\n", $items);

            $slackData = [
                "Restaurant" => $restaurantName->name,
                "Phone" => $order->restaurant()->phone_number,
                "Items" => $items
            ];

            Notification::send(User::first(), new SlackLunch($slackData));

            broadcast(new OrdersEvent(1, "complete"));
            return new JsonResponse("Order was submited and sent to slack!", 200);
        }
        abort(400, 'Order was already submited!.');
    }

    /**
     * @param int $itemId
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteOrder(int $itemId): JsonResponse
    {
        // Checks if itemId was provided
        if (empty($itemId)) {
            throw new \Exception("Item id was not provided");
        }

        // Delets order
        if (OrderItem::destroy($itemId)) {
            broadcast(new OrdersEvent($this->returnData(), "delete"));
            return new JsonResponse(["message" => "Item was successfully deleted", "data" => $this->returnData()], 200);
        }
        abort(400, "Error when deleting order!");
    }
}
