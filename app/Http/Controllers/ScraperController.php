<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;

abstract class ScraperController extends Controller
{
    public $restaurantName;
    public $url;
    public $id;
    public $passed;
    public $dailyItems;

    public $JsonFile = __DIR__."/restaurantsScraper/menuItems.json";

    /**
     * The restaurant name, that is passed to the constructor
     * will check if given restaurant exits and store restaurants data
     * from a database to global variables
     */
    public function __construct(string $restaurantName)
    {
        $this->restaurantName = $restaurantName;

        $data = Restaurant::where('name', $this->restaurantName)->get();

        if (!$data->isEmpty()) {
            $this->url = $data[0]->url;
            $this->restaurantName = $data[0]->name;
            $this->phoneNumber = $data[0]->phone;
            $this->id = $data[0]->id;

            $this->passed = true;

            return 1;

        }
        $this->passed = false;
        return 0;
    }

    /**
     * Receives an array of items and stores them to database
     * @param array $items an array of items [data, item, price];
     * @return bool
     */
    abstract public function addDataToDatabase(array $items, $id);

    /**
     * Web scrapper - scraps data from website and formats them correctly
     * @return array returns new data
     */
    abstract public function scrapTodaysItems();


    /**
     * Check if data was already inserted into database
     */
    abstract public function checkIfAlreadyInserted($id): bool;

    /**
     * Inserts data to JSON file if existing data is not the same as a new one
     * @param string $data data that is scraped with a scraper
     */
    abstract public function appendDataToJSON(string $data): bool;
}
