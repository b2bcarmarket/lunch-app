<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Updates database depending on the type
     * @param string $type
     * @param string $data
     * @param int $id
     */
    public function updateData(string $type, string $data, int $id)
    {
        switch ($type) {
            case "role":
                User::find($id)->syncRoles([$data]);
                break;
            default:
                User::where('id', $id)->update([$type => $data]);
                break;
        }
    }

    /**
     * Gets all users
     * @return JsonResponse
     */
    public function getUsers(): JsonResponse
    {
        return new JsonResponse([
            "response" => User::with('roles')->get(),
            "roles" => Role::all()->pluck('name')
        ]);
    }

    /**
     * Deletes user from database
     * @param $id
     * @return JsonResponse
     */
    public function deleteUser(int $id): JsonResponse
    {
        User::find($id)->delete();

        return new JsonResponse([
            "response" => "User was deleted!"
        ]);
    }

    /**
     * Modify user's data
     * @param Request $request
     * @return JsonResponse
     */
    public function updateUsers(Request $request): JsonResponse
    {
        $data = (array)$request->input('data');

        try {
            // Loops through data and updates database
            foreach ($data as $change) {
                $this->updateData($change["type"], $change["data"], $change["id"]);
            }

            return new JsonResponse([
                "response" => "Data was updated successfully!"
            ]);
        } // If there is an error
        catch (\Exception $e) {
            abort(403, $e);
        }
    }

    /**
     * Adds new user
     * @param Request $request
     * @return JsonResponse
     */
    public function addUser(Request $request): JsonResponse
    {
        $nameAndSurname = (string)$request->input('nameAndSurname');
        $email = (string)$request->input('email');
        $password = (string)$request->input('password');
        $role = (string)$request->input('role');

        // Checks if user filled all the fields
        if (!$request->filled(['nameAndSurname', 'email', 'password', 'role'])) {
            abort(400, "Some of the data is missing!");
        }

        // Creates new user and saves it
        $user = (new User)->fill([
            "email" => $email,
            "password" => hash::make($password),
            "name" => $nameAndSurname
        ]);

        $user->assignRole($role);
        $user->save();

        return new JsonResponse([
            "response" => "New user was created!"
        ]);
    }

    /**
     * Checks if user is authenticated
     * @param Request $request
     * @return JsonResponse
     */
    public function checkUser(Request $request): JsonResponse
    {
        // Returns users info and validation confirmation
        return new JsonResponse([
            'valid' => auth()->check(),
            'user' => auth()->user(),
            'role' => auth()->user()->hasRole('admin')
        ]);
    }

    /**
     * Updates users password
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePassword(Request $request): JsonResponse
    {
        $currentPassword = (string)$request->input('currentPassword');
        $newPassword = (string)$request->input('newPassword');
        $newPasswordAgain = (string)$request->input('newPasswordAgain');


        // Checks if user filled all the fields
        if (!$request->filled(['currentPassword', 'newPassword', 'newPasswordAgain'])) {
            abort(400, "Some of the data is missing!");
        }

        // Checks if new password and repeated new password are the same
        if ($newPassword != $newPasswordAgain) {
            abort(400, "New password and repeated password and not the same!");
        }

        // Checks if current password is the same as new one
        if ($newPassword == $currentPassword) {
            abort(400, "Current password and new password should not be the same!");
        }

        $user = auth()->user();

        // Check if provided password is same as current password
        if (!Hash::check($currentPassword, $user->password)) {
            abort(400, "Provided password doesn't match!");
        }

        // Updates password with new password
        $user->password = Hash::make($newPassword);
        $user->save();

        return new JsonResponse([
            "response" => "Password was successfully changed!"
        ]);
    }
}
