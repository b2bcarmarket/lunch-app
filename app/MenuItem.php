<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MenuItem
 * @package App
 *
 * @property int id
 * @property string item
 * @property float price
 * @property int restaurant_id
 */
class MenuItem extends Model
{
    protected $dateFormat = "Y-m-d";
    protected $dates = [
        'created_at'
    ];
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class)->select(array('id', 'name', 'phone_number'));
    }
}
