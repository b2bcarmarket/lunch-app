<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Http\Controllers\restaurantsScraper\GastroscraperController;
use App\Http\Controllers\restaurantsScraper\VinkascraperController;
use App\Http\Controllers\restaurantsScraper\FemecscraperController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            /**
             * IMPORTANT
             * When adding a new scrapper for restaurant,
             * name of the restaurant that is passed to the constructor
             * needs to be the same as restaurant name in database
             */

            /**
             * Initializes scrapers for restaurants and calls scraper,
             * that will scrap items from 9.00 to 12.00 CET.
            */
            $gastro = new GastroscraperController("gastro");
            $gastro->scrapTodaysItems();

            $vinka = new VinkascraperController("vinka");
            $vinka->scrapTodaysItems();

            $femec = new FemecscraperController("femec");
            $femec->scrapTodaysItems();
        })->everyMinute();/*everyFiveMinutes()->timezone('Europe/Ljubljana')->between('9.00', '12.00');*/

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
