<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrdersEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $value;
    public $action;

    /**
     * OrdersEvent constructor.
     * @param $value
     * @param $action
     * @return void
     */
    public function __construct($value, $action)
    {
        $this->action = $action;
        $this->value = $value;
    }

    /**
     * Get the channels the event should broadcast on.
     */
    public function broadcastOn()
    {
        return new Channel('Order');
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ["return" => $this->value];
    }
    /**
     * Determine if this event should broadcast.
     *
     * @return bool
     */
    public function broadcastAs(){
        switch ($this->action) {
            case "delete":
                return "orderDelete";
            case "complete":
                return "orderComplete";
            case "orders":
                return "orders";
        }
    }
}
