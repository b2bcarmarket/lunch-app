<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
USe Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class SlackLunch extends Notification
{
    use Queueable;

    private $task;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task)
    {
        $this->task = $task;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack()
    {
        $restaurant = $this->task["Restaurant"];
        $phone = $this->task["Phone"];
        $data = $this->task["Items"];
        return (new SlackMessage) ->success()
            ->attachment(function($attachment) use($restaurant, $phone, $data){
                $attachment->title(sprintf("%s - Tel. number: %s", strtoupper($restaurant), $phone))
                ->content($data);
            });
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
