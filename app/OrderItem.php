<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderItem
 * @package App
 *
 * @property int id
 * @property stirng comment
 * @property int order_id
 * @property int menu_item_id
 * @property int user_id
 */
class OrderItem extends Model
{
    protected $dateFormat = "Y-m-d";

    protected $fillable = [
        "comment",
        "menu_item_id",
        "user_id"
    ];

    public function menu_item()
    {
        return $this->belongsTo(MenuItem::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
