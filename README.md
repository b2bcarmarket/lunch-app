#Preskok lunch app

## Project Setup

```bash
npm install
```
```bash
Composer install && npm run production
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

```bash
php artisan jwt:secret
```

```bash
php artisan storage:link
```

```bash
php artisan jwt:secret
```

## Pusher setup
Go to https://pusher.com/, register and create a new channel.

Go to 'App keys' and generate new key and secret.

Paste received data to .env (Pusher section)

``` dotenv
PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=
```
## Slack
Get Slack's webhook url and paste it to .env 

``` dotenv
SLACK_WEBHOOK_URL=
```
## Migrations
```bash
php artisan db:migrate
```

```bash
php artisan db:seed
```

## Contributing

Maintain by Vid Bukovec




