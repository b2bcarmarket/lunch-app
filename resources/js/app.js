require('./bootstrap');
window.Vue = require('vue');

import App from './app.vue'
import Vuetify from '../plugins/vuetify.js'
import Router from './App/Router/router'
import Store from './App/Store/index.js'

const app = new Vue({
    el: '#app',
    vuetify: Vuetify,
    store: Store,
    router: Router,
    render: h => h(App)
});
