import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import api from '../Services/api'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        //Basic user data (id, name, email)
        user: {
            name: null,
            email: null,
            loginStatus: false,
            id: null,
            role: null
        },

        // Delete user dialog
        deleteUserDialog: false,

        // Order response
        orderResponse: null,
        orderResponseText: null,
        orderResponseIcon: null,
        orderResponseColor: null,

        //Admin response
        adminResponse: null,
        adminResponseText: null,
        adminResponseIcon: null,
        adminResponseColor: null,

        changes: [],
        ordersArray: [],
        restaurantName: "",
        enable_user_profile_dialog: false,

        // Change password
        messageType: null,
        responseMessage: '',

        // Admin panel
        users: [],
        roles: []
    },
    mutations: {
        GET_USERS(state, payload) {
            state.users = payload.users
            state.roles = payload.roles
        },
        UPDATE_CREDENTIALS(state, payload) {
            state.user.name = payload.name
            state.user.email = payload.email
            state.user.id = payload.id
            state.user.loginStatus = true
            state.user.role = payload.roles[0].name
        },
        UPDATE_ORDERS_ARRAY(state, payload) {
            if (payload.length != 0) {
                state.ordersArray = [];
                payload.forEach(element => state.ordersArray.push(element))
            } else {
                state.ordersArray = [];
            }
        },
        CLEAR_ORDER(state) {
            state.ordersArray = []
        },
        UPDATE_RESTAURANT_NAME(state, payload) {
            state.restaurantName = payload
        },
        UPDATE_MESSAGE_DATA(state, payload) {
            state.orderResponse = payload.orderResponse
            state.orderResponseText = payload.orderResponseText
            state.orderResponseIcon = payload.orderResponseIcon
            state.orderResponseColor = payload.orderResponseColor
        },

        /**
         * Updates message and message type
         * @param state
         * @param payload
         * @constructor
         */
        UPDATE_PASSWORD_RESPONSE(state, payload) {
            state.responseMessage = payload.responseMessage
            state.messageType = payload.status
            state.enable_user_profile_dialog = !payload.status
        },
        UPDATE_CHANGE_ARRAY(state, payload) {
            if (state.changes.length == 0) {
                state.changes.push(payload)
            }

            state.changes.forEach((element, index) => {
                if (!(element.id == payload.id) || !(element.type == payload.type)) {
                    state.changes.push(payload)
                }
            })
        }
    },
    actions: {
        //Updates Vuex state with data that is stored in localstorage
        updateCredentials({commit}) {
            if (localStorage.getItem('authToken') != null) {
                Axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('authToken')}`
                api.checkUserToken()
                    .then((response) => {
                        if (!response.data.valid) {
                            localStorage.clear();
                            window.location.href = "/login"
                        } else {
                            commit('UPDATE_CREDENTIALS', response.data.user)
                        }
                    })
            }
        },

        //Updates orders array
        modifyOrders({commit}, payload) {
            commit('UPDATE_ORDERS_ARRAY', payload)
        },
        clearOrderArray({commit}) {
            commit('CLEAR_ORDER')
        },
        updateRestaurantName({commit}, payload) {
            commit('UPDATE_RESTAURANT_NAME', payload);
        },
        updateMessageData({commit}, payload) {
            commit('UPDATE_MESSAGE_DATA', payload)
        },

        /**
         * Updates state with response when user changes password
         * @param commit
         * @param data
         */
        setPasswordResponse({commit}, data) {
            commit('UPDATE_PASSWORD_RESPONSE', data)
        },
        addChangeToArray({commit}, payload) {
            commit('UPDATE_CHANGE_ARRAY', payload)
        },

        // Get all users
        getUsers({commit}) {
            api.getUsers()
                .then((response) => {
                    commit('GET_USERS', {
                        users: response.data.response,
                        roles: response.data.roles
                    })
                })
        }
    }
})
