import Axios from 'axios'

const instance = Axios.create({
    headers: {
        'Authorization': `Bearer ` + localStorage.getItem('authToken')
    }

})

export default {

    /**
     * Gets data for specific restaurant
     * @param {STRING} name restaurant name
     */
    getRestaurantData(id) {
        return instance.get(`/api/restaurants/${id}/menu-items`)
    },

    getAllRestaurants() {
        return instance.get('/api/restaurants')
    },

    /**
     * Logs in user
     * @param {string} email
     * @param {string} password
     */
    loginUser(email, password) {
        return instance.post('/api/users/login', {email: email, password: password})
    },

    /**
     * Check if user's token is valid
     */
    checkUserToken() {
        return instance.post('/api/users/check')
    },

    /**
     * Sends request to database, to add data to order table
     * @param {string} userid user id
     * @param {object} itemdata item data
     * @param {object} comment user provided comment
     */
    addOrder(userid, itemdata, comment) {
        return instance.put(`/api/users/${userid}/orders`, {item: itemdata, comment: comment})
    },

    /**
     * Get all orders for today
     */
    getOrdersForToday() {
        return instance.get('/api/users/orders');
    },

    /**
     * Submits order
     * @param userId
     * @param restaurant
     */
    completeOrder(userId, restaurant) {
        return instance.patch(`/api/users/${userId}/orders`, {restaurantName: restaurant})
    },

    /**
     * Sends request to backend to delete item
     * @param {number} itemId
     */

    deleteItem(itemId) {
        return instance.delete(`/api/users/orders/${itemId}`)
    },

    /**
     * Update users password
     * @param userId
     * @returns {Promise<AxiosResponse<any>>}
     */
    updateUserPassword({
            currentPassword,
            newPassword,
            newPasswordAgain
    })
    {
        return instance.patch(`/api/users/password`,
            {
               currentPassword,
               newPassword,
                newPasswordAgain
            })
    },

    /**
     * Gets all users
     * @returns {Promise<AxiosResponse<any>>}
     */
    getUsers(){
        return instance.get('/api/users')
    },

    /**
     * Adds new user
     * @returns {Promise<AxiosResponse<any>>}
     * @param data
     */
    addUser(data){
        return instance.post('/api/users', data)
    },

    /**
     * Deletes use
     * @param {number} id
     */
    deleteUser(id){
        return instance.delete('/api/users/'+id)
    },

    /**
     * Modifies users data
     * @param {array} data
     * @returns {Promise<AxiosResponse<any>>}
     */
    modifyUsers(data){
        return instance.patch('/api/users/', {data: data})
    }
}
