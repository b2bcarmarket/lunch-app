import Vue from 'vue';
import VueRouter from 'vue-router'
import api from "../Services/api";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            component: ()=> import('../Pages/login/login')
        },
        {
            path: '/',
            redirect: '/restaurants',
            component: () => import('../Layouts/index.vue'),
            children: [
                {
                    path: '/restaurants',
                    component: () => import('../Pages/Restaurants/Layout'),
                    children:[
                        {
                            path: '',
                            component: ()=>import('../Pages/Restaurants/List')
                        },
                        {
                            path: ':id',
                            component: () => import('../Pages/Restaurants/restaurant')
                        },
                    ]
                },
                {
                    path: '/admin',
                    component: () => import('../Layouts/AdminPanel'),
                    // Before entering admin page it checks if user is admin
                    beforeEnter: (to, from, next) => {
                        api.checkUserToken()
                            .then((response)=>{
                                if(response.data.role){
                                    next()
                                }
                                else{
                                    next('/')
                                }
                            })
                    }
                }
            ]
        },
    ]
})

// Checks if user is logged in before entering path
router.beforeEach((to, from, next) => {
    if (to.path != '/login' && localStorage.getItem('authToken') == null) {
        next({path: '/login'})
    } else {
        next()
    }
})
export default router;

