import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
    theme: {
        themes:{
            light:{
                primary: '#9a0007',
                secondary: '#d32f2f',
                accent: '#E1E2E1',
                info: '#BF9663'
            }
        }
    }
})
