<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([

            [
                'name' => 'vinka',
                'phone_number' => '0599 26843',
                'url'=>'https://www.vinka.si/malice-in-kosila.html',
                'was_updated' => false
            ],
            [
                'name' => 'gastro',
                'phone_number' => '(01) 320 72 92 ',
                'url'=>'https://www.gastrohouse.si/index.php/tedenska-ponudba',
                'was_updated' => false
            ],
            [
                'name' => 'femec',
                'phone_number' => '041 635 101',
                'url' => 'http://www.femec.si/',
                'was_updated' => false
            ]
        ]);
    }
}
