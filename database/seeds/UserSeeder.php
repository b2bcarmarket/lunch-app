<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            [
                'name' => 'Jan Mrhar',
                'email' => 'jan.mrhar@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'Andraž Bajec',
                'email' => 'andraz@preskok.net',
                'password' => Hash::make('test1')
            ],
            [
                'name' => 'Staš Strozak',
                'email' => 'stas.strozak@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'Jan Kužnik',
                'email' => 'jan.kuznik@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'Borut Penko',
                'email' => 'borut.penko@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'Lovro Tramšek',
                'email' => 'lovro.tramsek@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'Tadej Miklavčič',
                'email' => 'tadej.miklavcic@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'Kristjan Hoareau',
                'email' => 'kristjan.hoareau@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'Frane Gorjanc',
                'email' => 'frane.gorjanc@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'David Rudman',
                'email' => 'david.rudman@preskok.si',
                'password' => Hash::make('test')
            ],
            [
                'name' => 'Matija Kogovšek',
                'email' => 'matija.kogovsek@preskok.si',
                'password' => Hash::make('test')
            ],
        ]);
    }
}
