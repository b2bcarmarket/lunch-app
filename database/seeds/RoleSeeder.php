<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Users ids who
        (array)$adminUsers = ['lovro.tramsek@preskok.si'];

        // Create roles
        $adminRole = Role::create(['name' => 'admin']);
        $adminPermission = Permission::create(['name' => 'edit users']);

        $adminRole->givePermissionTo($adminPermission);
        $adminPermission->assignRole($adminRole);

        $userRole = Role::create(['name' => 'user']);

        foreach (User::all() as $user) {
            $user->assignRole(in_array($user->email, $adminUsers) ? 'admin' : 'user');
        }
    }
}
